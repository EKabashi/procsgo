<!DOCTYPE html>
<html>
<head>
	<title>ProCsGo</title>
	<link rel="stylesheet" type="text/css" href="mycss.css">
</head>
<body>
	<?php 
			include("header.php"); 
	?>
	 
	<div class="container">
	
		<div class="maincontainer" style="margin-left: 180px;">

		<div class="leftad"></div>


		<div class="main">
			<div class="left">
				<div class="match"><a href=""><img src="match.jpg"></a></div>
				<div class="potw">Player of the week</br><a href=""><img src="potw.jpg"></a></div>
				<div class="bet"><a href=""><img src="bet.jpg"></a></div>
				<div class="rank">
					Ranking </br>
					<div class="r"><a href="">1.Astralis</a></div>
					<div class="r"><a href="">2.Liquid</a></div>
					<div class="r"><a href="">3.Natus Vincere</a></div>
					<div class="r"><a href="">4.Faze</a></div>
					<div class="r"><a href="">5.ENCE</a></div>
					<div class="r"><a href="">Last Update:6th May</a></div>
				</div>
				<div class="event">
					Events</br>
					<div class="e1"><a href="">ESL E9 Europe</a></div>
					<div class="e1"><a href="">ESL E9 Americas</a></div>
					<div class="e1"><a href="">BLAST Madrid</a></div>
					<div class="e1"><a href="">DH Tours 2019</a></div>
					<div class="e1"><a href="">cs_summit 4</a></div>
				</div>
				<div class="gall">
					Galleries</br>
					<div class="gall1"><a href=""><img src="gall.jpeg"></a></div>
					<div class="gall2"><a href="">Copenhagen Games 2018 - Day 1</a></div>
				</div>
				<div class="ad3"><a href=""><img src="ad3.jpg"></a></div>
			</div>
			<div class="center">
				<div class="centerp">
					<div class="slideshow-container">

  <!-- Full-width images with number and caption text -->
  <div class="dotsbox">
<!-- The dots/circles -->
<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(3)"></span>
</div>
</div>

  <div class="mySlides fade">
    <a href="https://www.hltv.org/news/26694/valve-set-dates-for-2020-and-2021-majors"><img src="https://i.ibb.co/SJKKjMx/1.jpg" style="width:100%"></a>
  </div>

  <div class="mySlides fade">
    <a href="https://www.hltv.org/news/26684/get-right-weve-been-working-our-asses-off-with-this-core-and-we-want-to-be-successful-together">
    <img src="https://i.ibb.co/SmZr8rZ/2.jpg" style="width:100%"></a>
  </div>

  <div class="mySlides fade">
    <a href="https://www.hltv.org/news/26664/a-year-at-the-summit-how-astralis-wrote-history"><img src="https://i.ibb.co/hYzhC42/3.jpg" style="width:100%"></a>
  </div>

</div>
<br>

				</div>



				<div class="news1">
					Today News </br>
					<div class="tn">
							<div class="tn1"><p><a href="">mertz: "I definitely think blameF is the leader"</a></p></div>
							<div class="tn1"><p><a href="">BLAST Pro Series Madrid Fantasy goes live</a></p></div>
							<div class="tn1"><p><a href="">Cloud9 to attend BLAST Pro Series Los Angeles</a></p></div>
							<div class="tn1"><p><a href="">Video: Twistzz - MVP of IEM Sydney 2019</a></p></div>				
							<div class="tn1"><p><a href="">DreamHack Open Tours on-air team announced</a></p></div>
							<div class="tn1"><p><a href="">IEM Sydney Fantasy recap</a></p></div>
				
					</div>
				</div>
					<div class="news1">
					Yesterday News </br>
					<div class="tn">
							<div class="tn1"><p><a href="">mertz: "I definitely think blameF is the leader "</a></p></div>
							<div class="tn1"><p><a href="">BLAST Pro Series Madrid Fantasy goes live</a></p></div>
							<div class="tn1"><p><a href="">Cloud9 to attend BLAST Pro Series Los Angeles</a></p></div>
							<div class="tn1"><p><a href="">Video: Twistzz - MVP of IEM Sydney 2019</a></p></div>				
							<div class="tn1"><p><a href="">DreamHack Open Tours on-air team announced</a></p></div>
							<div class="tn1"><p><a href="">IEM Sydney Fantasy recap</a></p></div>
				
					</div>
				</div>
					<div class="news1">
					Previous News </br>
					<div class="tn">
							<div class="tn1"><p><a href="">mertz: "I definitely think blameF is the leader"</a></p></div>
							<div class="tn1"><p><a href="">BLAST Pro Series Madrid Fantasy goes live</a></p></div>
							<div class="tn1"><p><a href="">Cloud9 to attend BLAST Pro Series Los Angeles</a></p></div>
							<div class="tn1"><p><a href="">Video: Twistzz - MVP of IEM Sydney 2019</a></p></div>				
							<div class="tn1"><p><a href="">DreamHack Open Tours on-air team announced</a></p></div>
							<div class="tn1"><p><a href="">IEM Sydney Fantasy recap</a></p></div>
				
					</div>

				</div>
			</div>
			<div class="right">
				<div class="tm">
					<div class="tm1">Today matches</div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="matcht"><a href="">T1vsT2</a></div>
					<div class="ad4"><a href=""><img src="ad4.jpg"></a></div>
				</div>
				<div class="rr">
					<div class="ra">
						<div>Recent Activity</div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
						<div class="ra1"><a href="">recent</a></div>
					</div>
					<div class="lr">
						<div>Latest Replays</div>
						<div class="lr1"><a href="">Replay DEMO</a></div>
						<div class="lr1"><a href="">Replay DEMO</a></div>
						<div class="lr1"><a href="">Replay DEMO</a></div>
						<div class="lr1"><a href="">Replay DEMO</a></div>
						<div class="lr1"><a href="">Replay DEMO</a></div>
						<div class="lr1"><a href="">Replay DEMO</a></div>
						<div class="lr1"><a href="">Replay DEMO</a></div>
						<div class="lr1"><a href="">Replay DEMO</a></div>
						<div class="lr1"><a href="">Replay DEMO</a></div>
						<div class="lr1"><a href="">Replay DEMO</a></div>
						<div class="lr1"><a href="">Replay DEMO</a></div>
					</div>
				</div>
			</div>
		</div>

		<div class="rightad"></div>
	</div>

</div>



<script>
var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}    
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 6000); // mrenda sa sekondave me ndryshu
}
	</script>

</body>
</html>